﻿using NUnit.Framework;

namespace TeamCityDemoProject.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void Should_return_sum_from_addition()
        {
            var calculator = new Calculator();
            var actual = calculator.Add(2, 2);

            Assert.That(actual, Is.EqualTo(4));
        }

        [Test]
        public void Should_return_sum_from_substraction()
        {
            var calculator = new Calculator();
            var actual = calculator.Substract(5, 2);

            Assert.That(actual, Is.EqualTo(3));
        }

        [Test]
        public void Should_return_sum_from_divided()
        {
            var calculator = new Calculator();
            var actual = calculator.Divide(5, 5);

            Assert.That(actual, Is.EqualTo(1));
        }
    }
}