﻿namespace TeamCityDemoProject
{
    public class Calculator
    {
        public int Add(int first, int second)
        {
            return first + second;
        }

        public int Substract(int first, int second)
        {
            return first - second;
        }

        public int Divide(int first, int second)
        {
            return first/second;
        }
    }
}